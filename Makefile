.SILENT:

.PHONY: run
run:
	PYTHONPATH=. python3 src/main.py FlDOZ 2000
.PHONY: test
test:
	PYTHONPATH=. pytest
