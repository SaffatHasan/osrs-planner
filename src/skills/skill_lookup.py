from src.skills.skills import SkillsEnum, Skill
from OSRSBytes import Hiscores

def get_total_level(userName: str):
    user = getUser(userName)

    return sum(user.skill(skill.name, 'level') for skill in SkillsEnum)


def getXPToNextLevel(userName: str) -> {str: Skill}:
    user = getUser(userName)
    return {
        skill.name: Skill(
            skill.name, 
            user.skill(skill.name, 'level'),
            user.skill(skill.name, 'exp_to_next_level'),
        )
        for skill in SkillsEnum if user.skill(skill.name, 'level') < 99 
    }

def getUser(userName: str):
    userName = userName.replace(' ', '%A0')
    return Hiscores(userName)
    
