import dataclasses
import enum

class SkillsEnum(enum.Enum):
    attack=0
    defense=1
    strength=2
    hitpoints=3
    ranged=4
    prayer=5
    magic=6
    cooking=7
    woodcutting=8
    fletching=9
    fishing=10
    firemaking=11
    crafting=12
    smithing=13
    mining=14
    herblore=15
    agility=16
    thieving=17
    slayer=18
    farming=19
    runecrafting=20
    hunter=21
    construction=22

@dataclasses.dataclass
class Skill:
    name: str
    current_level: int
    xp_remaining: int

    def level_up(self):
        new_level = self.current_level + 1
        return Skill(
            self.name,
            new_level,
            int((new_level + 300 * ( 2 ** (new_level/7)))/4),
        )

    def __hash__(self):
        return hash((self.name, self.current_level, self.xp_remaining))

    def __repr__(self):
        return f"Skill(name={self.name}, current_level={self.current_level}, xp_remaining={self.xp_remaining})"
