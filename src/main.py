from src.training.calculator import calculate_optimal_path
import sys

def main():
    user_name = sys.argv[1]
    target_level = int(sys.argv[2])
    optimal_path = calculate_optimal_path(user_name, 2000)
    print_table(user_name, target_level, optimal_path)

def print_table(user_name, target_level, optimal_path):
    print("{:^15}|{:^15}|{:^15}".format("Skill", "# Levels", "Efficient Hours"))
    print("-"*45)
    for key in optimal_path:
        print(f"{key:<15}|{len(optimal_path[key]):^15}|{sum(optimal_path[key]):^15.2f}")
    print("-"*45)
    print(f"{user_name} needs {sum(sum(optimal_path[key]) for key in optimal_path):.2f} hours to reach {target_level} total level")

if __name__ == "__main__":
    main()