import yaml

def load_methods():
     return load_from_file('methods.yml')

def load_from_file(file_path):
     with open(f"src/resources/{file_path}") as f:
          data = f.read()
     return yaml.load(data, Loader=yaml.SafeLoader)