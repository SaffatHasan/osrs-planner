from src.training.util import load_methods
from src.skills.skills import Skill
from src.skills.skill_lookup import getXPToNextLevel, get_total_level
from collections import defaultdict

methods = load_methods()

def calculate_optimal_path(user_name: str, target_level: int):
    efficient_hours_to_level = calc_efficient_hours(user_name)
    current_total = get_total_level(user_name)

    shit_to_level = defaultdict(lambda: [])
    for _ in range(target_level - current_total):
        min_skill = get_min_skill(efficient_hours_to_level)
        shit_to_level[min_skill.name].append(efficient_hours_to_level[min_skill])
        efficient_hours_to_level = increase_by_one(efficient_hours_to_level, min_skill)
    return shit_to_level

def increase_by_one(efficient_hours_to_level, skill_to_level):
    new_min_skill = skill_to_level.level_up()

    efficient_hours_to_level[new_min_skill] = calc_efficient_hour(new_min_skill)
    efficient_hours_to_level.pop(skill_to_level)
    return efficient_hours_to_level

def get_min_skill(dictionary):
    return min(dictionary, key=dictionary.get)

def calc_efficient_hours(user_name):
    xpToNextLevel = getXPToNextLevel(user_name)
    return {
        xpToNextLevel[skill]: calc_efficient_hour(xpToNextLevel[skill]) for skill in methods if skill in xpToNextLevel
    }

def calc_efficient_hour(skill):
    return skill.xp_remaining / methods[skill.name]['XPH']

def calc_xp_to_next_level(currentLevel):
    return int((currentLevel + 300 * ( 2 ** (currentLevel/7)))/4)
