import unittest
from src.skills.skills import SkillsEnum
from src.skills.skill_lookup import get_total_level

class TestSkillLookup(unittest.TestCase):
    def test_lookup(self):
        userName = 'Lynx Titan'

        actual = get_total_level(userName)
        expected = 2277

        assert actual == expected
