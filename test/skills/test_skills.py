import unittest
from src.skills.skills import SkillsEnum, Skill

class TestSkills(unittest.TestCase):
    def test_naming(self):
        assert SkillsEnum.attack.name == 'attack'
        assert SkillsEnum.attack.value == 0

    def test_skill(self):
        rc = Skill('runecrafting', 68, 999)

        assert rc.xp_remaining == 999

        new = rc.level_up()

        assert new.xp_remaining == 69576
        assert new.current_level == 69
