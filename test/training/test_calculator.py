import unittest
from src.training import calculator
from src.skills.skills import Skill

class TestCalculator(unittest.TestCase):
    def test_xp_to_level(self):
        level = 69
        actual = calculator.calc_xp_to_next_level(level)
        expected = 69576

        assert actual == expected

    def test_get_min(self):
        xp_to_next = {
            Skill(
                'foo',
                69,
                1000,
            ): 999,
            Skill(
                'bar',
                50,
                100,
            ): 100,
            Skill(
                'baz',
                50,
                999,
            ): 999,
        }

        actual = calculator.get_min_skill(xp_to_next)
        expected = Skill(
            'bar',
            50,
            100,
        )

    def test_increase(self):
        xp_to_next = {
            Skill('foo', 69, 10): 999,
            Skill('cooking', 50, 1): 100,
            Skill('baz', 50, 10): 999,
        }

        actual = calculator.increase_by_one(xp_to_next, calculator.get_min_skill(xp_to_next))

        expected = {
            Skill('foo', 69, 10): 999,
            Skill('cooking', 51, 11715): 0.019525,
            Skill('baz', 50, 10): 999,
        }

        print(actual)
        print(expected)

        assert actual == expected